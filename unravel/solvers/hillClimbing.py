#!/usr/bin/env python
# encoding: utf-8

"""
The hill-climbing solver module for Unravel.

Written by nqngo, kbawa (2012).
"""

import random
import time
import json
import nltk
from nltk import FreqDist
from math import log10
from copy import deepcopy


class Solver(object):
    
    def __init__(self, params, alphabets, cipherglyph):
        """Initialise the object with the given parameters"""
        # Get intial parameters
        self._params = params
        # Alphabet of the ciphertext
        self._cipherglyph = unicode(cipherglyph)
        # Alphabet of the plaintext
        self._alphabets = unicode(alphabets)
        # mapping to different fitness functions
        self._fitnessF = {'2gram':self.biGramLogFitness,
                          '3gram':self.triGramLogFitness,
                          '4gram':self.fourGramLogFitness}
        # Load all the model statistic data
        with open(self._params['stat1f'], 'r') as unigram:
            self._stat1 = json.load(unigram)
        with open(self._params['stat2f'], 'r') as bigram:
            self._stat2 = json.load(bigram)
        with open(self._params['stat3f'], 'r') as trigram:
            self._stat3 = json.load(trigram)
        with open(self._params['stat4f'], 'r') as fourgram:
            self._stat4 = json.load(fourgram)

        
        # Convert to log probability
        for k in self._stat1:
            self._stat1[k]=log10(self._stat1[k])
        for k in self._stat2:
            self._stat2[k]=log10(self._stat2[k])
        for k in self._stat3:
            self._stat3[k]=log10(self._stat3[k])
        for k in self._stat4:
            self._stat4[k]=log10(self._stat4[k])    


    def solve(self, ciphertext):
        """Solve the input ciphertext using hill climbing algorithm"""
        
        #Keep the timing
        self._start = time.time()
        
        cipher = unicode(ciphertext)
        # Save the ciphertext first
        self.cipher = cipher
        # Gather the statistic of the ciphertext
        self._cstat1 = FreqDist(cipher)
        self._cstat2 = FreqDist(''.join(e) for e in nltk.bigrams(cipher))
        self._cstat3 = FreqDist(''.join(e) for e in nltk.trigrams(cipher))
        self._cstat4 = FreqDist(''.join(e) for e in nltk.ngrams(cipher,4))
        
        # Get the initial key of the solver
        self.key = self.generateInitialKey(ciphertext)
        
        # Setup the algorithm
        head = 0
        tail = 0
        bestKey = self.key
        bestScore = self.evaluateScore(self.decode(cipher, self.key),
                                                self._fitnessF[self._params['fitness']])
        bestScore = float('-inf')
        
        # The main loop
        for i in range(int(self._params['iteration'])):
            # Check if it reaches the end, loop back
            if tail >= len(self._cipherglyph) - 1:
                # Rerun the algorithm after the loop back
                if head >= len(self._cipherglyph) - 2:
                    head = 0 
                else:
                    head += 1
                if (int(self._params['iteration'] == 1)):   
                    tail = head + 1
                else:
                    tail = 0
                self.key = bestKey
            else:
                tail += 1
    
            ## Permutate and evaluate key
            newKey = self.permutateKey(head, tail)
            newScore = self.evaluateScore(self.decode(cipher, newKey),
                                          self._fitnessF[self._params['fitness']])
            if(newScore > bestScore):
                bestScore = newScore
                bestKey = newKey
                print "Fitness = %.7f at Iteration=%d" % (bestScore, i)
                print "The result is:"
                print self.decode(cipher, bestKey)
                print "Taken %.4f seconds" % (time.time() - self._start)
                print


    def generateInitialKey(self, cipher):
        """Generate an initial key based on ciphertext unigram distribution"""
        # Sorted frequency of the ciphertext, consider all glyphs that
        # is in the cipher alphabet but does not exists in ciphertext
        fd = [(c, self._cstat1[c]) if c in self._cstat1
                                   else (c, 0)
                                   for c in self._cipherglyph]
        fd = sorted(fd, key=lambda x: x[1], reverse=True)
        # Sorted frequency of the alphabet, discard data not in alphabet
        sd = [e for e in self._stat1.items() if e[0] in self._alphabets]
        print sd
        sd = sorted(sd, key=lambda x: x[1], reverse=True)
        # Then return key as a dictionary
        return dict((c[0], k[0]) for (c, k) in zip(fd, sd))
          
        
    def permutateKey(self, head, tail):
        """Permutate a key using variation of bubble sort"""
        # Deep copy to stablise the current key
        key = deepcopy(self.key)
        # Get head glyph and tail glyph
        hglyph = self._cipherglyph[head]
        tglyph = self._cipherglyph[tail]
        key[hglyph], key[tglyph] = key[tglyph], key[hglyph]
        return key
        
        
    def decode(self, ciphertext, key):
        """Decode the ciphertext given the solver key"""
        return ''.join(c if c not in key else key[c]
                      for c in ciphertext)


    def evaluateScore(self, decoded, f):
        """
        Evaluate the score of a decoded text based on the given function.
        
        Possible func:
            naiveFitness - naive Fitness function with no weight
            SpillmanFitness - Fitness function by Spillman, R.
            weightedFitness - weighted Fitness function
        """
        return f(decoded)


    def biGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        bi-grams.
        """
        fd2 = FreqDist(''.join(e) for e in nltk.bigrams(decoded))
        return sum(self._stat2[k] if k in self._stat2
                   else float(self._params['floor'])
                   for k in fd2)

    def triGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        tri-grams.
        """
        fd3 = FreqDist(''.join(e) for e in nltk.trigrams(decoded))
        return sum(self._stat3[k] if k in self._stat3
                   else float(self._params['floor'])
                   for k in fd3)

    def fourGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        four-grams.
        """
        fd4 = FreqDist(''.join(e) for e in nltk.ngrams(decoded,4))
        return sum(self._stat4[k] if k in self._stat4
                   else float(self._params['floor'])
                   for k in fd4)

__DEBUG__ = False


# Debugging functions
if __DEBUG__ == True:
    #ciphers = 'ABCDDD'
    ciphers = 'CDUYEENJYNDZNDINKFQMUEMKEZCXDNQHKCUGYNECYEMQMXEMKCYEZCXNQEZUEAZMYNGMYNYCYEZMXAMFEQTGNREZCXNQEZUEMHMGMYECDUYEENJYNDZCXEZNTIZEXEZMQMXEUQMUHHKMEUCHXYUETQMUONTYKXDCEZAMQCNKCFAZMYNGMYURQNGEZMGNECNYNRXDCYIENEZMNXFCHHUECNYXNRUENGXNRFNTQXMYNEZCYICYYUETQMCXMVUFEHPAMQCNKCFUHHGNECNYZUXUOMICYCYIUYKUYMYKXNEZUECYEZMGUEZMGUECFUHXMYXMAMQCNKCFCEPKNMXYNEMVCXECYQMUHDNQHKYMLMQEZMHMXXAMQCNKCFCEPZUXAQNLMKENOMMVEQMGMHPTXMRTH'
    #ciphers = 
    alphabets = 'abcdefghijklmnopqrstuvwxyz'
    cipherglyphs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    params = {
        'fitness': '2gram',
        'iteration': '5000',
        'stat1f': '../../assets/war1.json',
        'stat2f': '../../assets/war2.json',
        'stat3f': '../../assets/war3.json',
        'stat4f': '../../assets/war4.json',
        'floor': '-50.0',
    }
    solver = Solver(params,alphabets, cipherglyphs)
    solver.solve(ciphers)    
