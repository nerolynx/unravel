#!/usr/bin/env python
# encoding: utf-8

"""
The genetic-climbing solver module for Unravel.

Written by kbawa (2012).
"""

import random
import json
import nltk
from nltk import FreqDist
from math import log10
from copy import deepcopy
import collections

class GeneticSolver(object):
    
    def __init__(self, params, alphabets, cipherglyph):
        """Initialise the object with the given parameters"""
        # Get intial parameters
        self._params = params
        # Alphabet of the ciphertext
        self._cipherglyph = unicode(cipherglyph)
        # Alphabet of the plaintext
        self._alphabets = unicode(alphabets)
        # mapping to different fitness functions
        self._fitnessF = {'oneGramLogF':self.oneGramLogFitness,
                          'biGramLogF':self.biGramLogFitness,
                          'triGramLogF':self.triGramLogFitness,
                          'fourGramLogF':self.fourGramLogFitness,
                          'Spillman':self.SpillmanFitness,}
        # Load all the model statistic data
        with open(self._params['stat1f'], 'r') as unigram:
            self._stat1 = json.load(unigram)
        with open(self._params['stat2f'], 'r') as bigram:
            self._stat2 = json.load(bigram)
        with open(self._params['stat3f'], 'r') as trigram:
            self._stat3 = json.load(trigram)
        with open(self._params['stat4f'], 'r') as fourgram:
            self._stat4 = json.load(fourgram)    
        
        # Convert to log probability
        for k in self._stat1:
            self._stat1[k]=log10(self._stat1[k])
        for k in self._stat2:
            self._stat2[k]=log10(self._stat2[k])
        for k in self._stat3:
            self._stat3[k]=log10(self._stat3[k])
        for k in self._stat4:
            self._stat4[k]=log10(self._stat4[k]) 


    def solve(self, ciphertext):
        """Solve the input ciphertext using hill climbing algorithm"""
        cipher = unicode(ciphertext)
        # Save the ciphertext first
        self.cipher = cipher
        # Gather the statistic of the ciphertext
        self._cstat1 = FreqDist(cipher)
        self._cstat2 = FreqDist(''.join(e) for e in nltk.bigrams(cipher))
        self._cstat3 = FreqDist(''.join(e) for e in nltk.trigrams(cipher))
        self._cstat4 = FreqDist(''.join(e) for e in nltk.ngrams(cipher,4))
        
        # Get the initial set of parent keys of the solver
        self.pool = self.generateInitialKey()
        
        while(iteration < int(self._params['iteration'])):
            iteration+=1
            self.pool = self.crossOverFunction(self.pool,ciphertext)
            
            
        for key in self.pool:
                print ''.join(v for (k,v) in key.items())
                print self.decode(ciphertext,key)


    def generateInitialKey(self):
        """Generate an initial set of keys"""
        i = 0
        keySet = []
        while(i < int(self._params['pool'])):
            newKey = ''.join(random.sample(cipherglyphs,len(cipherglyphs)))
            newKey = dict((c[0].upper(), k[0].lower()) for (c, k) in zip(list(self._cipherglyph),
                                                      list(newKey)))
            if newKey not in keySet:
                keySet.append(newKey)
                i+=1
        return keySet       

       
    def crossOverFunction(self,pool,ciphertext):
        """mate 2 randomly selected parent keys to from a pool of key"""
        sortedPool = []
        for key in pool:
            score = self.evaluateScore(self.decode(ciphertext, key),
                                       self._fitnessF[self._params['fitness']])
            sortedPool.append((score,key))
        sortedPool.sort(reverse=True)   
        
        # pick best parents from the above sorted list
        maxValue = (len(sortedPool)-len(sortedPool)%2)/2
        maxValue = maxValue + maxValue%2
        bestParents = sortedPool[:maxValue]
        childKeys = []

        '''
        # meet all parents
        for i in range(0,int(self._params['pool'])):
            for j in range(i+1,int(self._params['pool'])):
                if(i!=j):
                    parent1 = sortedPool[i]
                    parent2 = sortedPool[j]
                    child1 = self.mutateParents(parent1[1],parent2[1],False)
                    child2 = self.mutateParents(parent1[1],parent2[1],True)
                    # add the childs to final list
                    score = self.evaluateScore(self.decode(ciphertext, child1),
                                       self._fitnessF[self._params['fitness']])
                    childKeys.append((score,child1))
                    score = self.evaluateScore(self.decode(ciphertext, child2),
                                       self._fitnessF[self._params['fitness']])
                    childKeys.append((score,child2))
        '''
        while(bestParents != []):
            # randomly select 2 parents
            index1 = random.randrange( len(bestParents) )
            parent1 = bestParents.pop(index1)
            index2 = random.randrange( len(bestParents) )
            parent2 = bestParents.pop(index2)

            # find childs based on selected parents
            child1 = self.mutateParents(parent1[1],parent2[1],False)
            child2 = self.mutateParents(parent1[1],parent2[1],True)

            # add the childs to final list
            score = self.evaluateScore(self.decode(ciphertext, child1),
                                       self._fitnessF[self._params['fitness']])
            childKeys.append((score,child1))
            score = self.evaluateScore(self.decode(ciphertext, child2),
                                       self._fitnessF[self._params['fitness']])
            childKeys.append((score,child2))

        # mutate the child keys again
        self.mutate(childKeys,ciphertext)
        
        
        sortedPool+childKeys
        sortedPool.sort(reverse=True)
        
        # return the best keys 
        return ([key for (score,key) in sortedPool])[:int(self._params["pool"])]    

    
    
    def mutate(self,pool,ciphertext):
        """mutate the top and the bottom part"""
        # Take top half
        for i in range(0,int(self._params["pool"])/2):
            self._topmutate(pool[i][1])
            score = self.evaluateScore(self.decode(ciphertext, pool[i][1]),
                        self._fitnessF[self._params['fitness']])
            pool[i]=(score,pool[i][1])

        # Take bottom half cut off at len(pool)
        for key in range(int(self._params["pool"])/2,int(self._params["pool"])):
            self._bottommutate(pool[i][1])
            score = self.evaluateScore(self.decode(ciphertext, pool[i][1]),
                        self._fitnessF[self._params['fitness']])
            pool[i]=(score,pool[i][1])

    def _topmutate(self, key):
        """Mutate the top of half of the gene according to Spillman."""
        # Select two random elements on left half of the key to swap
        a = random.choice(self._cipherglyph[:len(self._cipherglyph)/2])
        b = random.choice(self._cipherglyph[:len(self._cipherglyph)/2])
        # make sure a and b are not same
        while(a==b):
            b = random.choice(self._cipherglyph[:len(self._cipherglyph)/2])
        key[a], key[b] = key[b], key[a]


    def _bottommutate(self, key):
        """Mutate the bottom of half of the gene according to Spillman."""
        # Select two random elements on left half of the key to swap
        a = random.choice(self._cipherglyph[len(self._cipherglyph)/2:])
        b = random.choice(self._cipherglyph[len(self._cipherglyph)/2:])
        # make sure a and b are not same
        while(a==b):
            b = random.choice(self._cipherglyph[:len(self._cipherglyph)/2])
        # Make the swap
        key[a], key[b] = key[b], key[a]
                        
    def mutateParents(self,key1,key2,reverse):
        """make a child from two parent key"""
        strKey1 = ''.join([v for (k,v) in key1.items()])
        strKey2 = ''.join([v for (k,v) in key2.items()])
        finalKey = []
        # find the key by mutating from right to left
        if(reverse == False):
            for i in range(0,len(key1)):
                freq_char1 = self._cstat1[strKey1[i].upper()] if strKey1[i].upper() in self._cstat1 else 0  
                freq_char2 = self._cstat1[strKey1[i].upper()] if strKey1[i].upper() in self._cstat1 else 0
                
                if(freq_char1 > freq_char2):
                    finalKey.append(strKey1[i].lower() if strKey1[i] not in finalKey
                                                      else '0')
                else:
                    finalKey.append(strKey2[i].lower() if strKey2[i] not in finalKey
                                                      else '0')        
            leftOverChar = [c.lower() for c in self._cipherglyph
                            if c.lower() not in ''.join(finalKey)]
                            
            # substitute the left over keys
            for i in range(0,len(finalKey)):
                if(finalKey[i] == '0'):
                    finalKey[i] = leftOverChar.pop(random.randrange( len(leftOverChar) ))
                    
            return dict((c[0].upper(), k[0].lower()) for (c, k) in zip(self._cipherglyph,
                                                      list(finalKey)))
        # find the key by mutating from right to left
        else:
            for i in reversed(range(0,len(key1))):
                freq_char1 = self._cstat1[strKey1[i].upper()] if strKey1[i].upper() in self._cstat1 else 0  
                freq_char2 = self._cstat1[strKey1[i].upper()] if strKey1[i].upper() in self._cstat1 else 0
                if(freq_char1 > freq_char2):
                    finalKey.append(strKey1[i].lower() if strKey1[i] not in finalKey
                                                      else '0')
                else:
                    finalKey.append(strKey2[i].lower() if strKey2[i] not in finalKey
                                                      else '0')                     
            leftOverChar = [c.lower() for c in self._cipherglyph
                            if c.lower() not in ''.join(finalKey)]
            # substitute the left over keys
            for i in range(0,len(finalKey)):
                if(finalKey[i] == '0'):
                    finalKey[i] = leftOverChar.pop(random.randrange( len(leftOverChar) ))
            return dict((c[0].upper(), k[0].lower()) for (c, k) in zip(list(self._cipherglyph),
                                                      list(finalKey)))
                    
            
    def permutateKey(self, head, tail):
        """Permutate a key using variation of bubble sort"""
        # Deep copy to stablise the current key
        key = deepcopy(self.key)
        # Get head glyph and tail glyph
        hglyph = self._cipherglyph[head]
        tglyph = self._cipherglyph[tail]
        key[hglyph], key[tglyph] = key[tglyph], key[hglyph]
        return key
        
        
    def decode(self, ciphertext, key):
        """Decode the ciphertext given the solver key"""
        return ''.join(c if c not in key else key[c]
                      for c in ciphertext)

    def evaluateScore(self, decoded, f):
        """
        Evaluate the score of a decoded text based on the given function.
        
        Possible func:
            naiveFitness - naive Fitness function with no weight
            SpillmanFitness - Fitness function by Spillman, R.
            weightedFitness - weighted Fitness function
        """
        return f(decoded)
    

    def oneGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        one-grams.
        """
        fd1 = FreqDist(decoded)
        return sum(self._stat1[k] if k in self._stat1
                   else float(self._params['floor'])
                   for k in fd1)

    def biGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        bi-grams.
        """
        fd2 = FreqDist(''.join(e) for e in nltk.bigrams(decoded))
        return sum(self._stat2[k] if k in self._stat2
                   else float(self._params['floor'])
                   for k in fd2)

    def triGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        tri-grams.
        """
        fd3 = FreqDist(''.join(e) for e in nltk.trigrams(decoded))
        return sum(self._stat3[k] if k in self._stat3
                   else float(self._params['floor'])
                   for k in fd3)

    def fourGramLogFitness(self, decoded):
        """
        Evaluate the log Fitness score based on log probability of
        four-grams.
        """
        fd4 = FreqDist(''.join(e) for e in nltk.ngrams(decoded,4))
        return sum(self._stat4[k] if k in self._stat4
                   else float(self._params['floor'])
                   for k in fd4)
    
    def SpillmanFitness(self, decoded):
        """
        Evaluate the fitness score naively weighting using scoring formula
        by Spillman:
        
        score = 1 - (sum(R1[k]-P1[k]) +sum(R2[k]-P2[k])/4)^8
        """
        # Statistic of the deciphered text
        fd1 = FreqDist(decoded)
        fd2 = FreqDist(''.join(e) for e in nltk.bigrams(decoded))

        # Return fitness score following Spillman's formula
        return (1.0 -  sum(self._stat1[k] - fd1.freq(k) if k in self._stat1
                       else -fd1.freq(k) for k in fd1) + \
                       sum(self._stat2[k] - fd2.freq(k) if k in self._stat2
                       else -fd2.freq(k) for k in fd2) / \
                       4.0)**8


__DEBUG__ = True


# Debugging functions
if __DEBUG__ == True:
    #ciphers = 'ABCDDD'
    ciphers = 'CDUYEENJYNDZNDINKFQMUEMKEZCXDNQHKCUGYNECYEMQMXEMKCYEZCXNQEZUEAZMYNGMYNYCYEZMXAMFEQTGNREZCXNQEZUEMHMGMYECDUYEENJYNDZCXEZNTIZEXEZMQMXEUQMUHHKMEUCHXYUETQMUONTYKXDCEZAMQCNKCFAZMYNGMYURQNGEZMGNECNYNRXDCYIENEZMNXFCHHUECNYXNRUENGXNRFNTQXMYNEZCYICYYUETQMCXMVUFEHPAMQCNKCFUHHGNECNYZUXUOMICYCYIUYKUYMYKXNEZUECYEZMGUEZMGUECFUHXMYXMAMQCNKCFCEPKNMXYNEMVCXECYQMUHDNQHKYMLMQEZMHMXXAMQCNKCFCEPZUXAQNLMKENOMMVEQMGMHPTXMRTH'
    #ciphers = 
    alphabets = 'abcdefghijklmnopqrstuvwxyz'
    cipherglyphs = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    params = {
        'fitness': 'triGramLogF',
        'pool':'10',
        'iteration': '50',
        'threshold': '0',
        'alpha': '0.1',
        'beta': '1.0',
        'gamma': '0.8',
        'stat1f': '../../assets/war1.json',
        'stat2f': '../../assets/war2.json',
        'stat3f': '../../assets/war3.json',
        'stat4f': '../../assets/war4.json',
        'floor': '-50.0',
    }               
    solver = GeneticSolver(params,alphabets, cipherglyphs)
    solver.solve(ciphers)
