#!/usr/bin/env python
# encoding: utf-8

"""
English phrase translation for the program.
"""

#Welcome message
welcome = """University of Melbourne: 
UNRAVEL: An automatic program for crytanalysing
                                mono-substitution cipher.
    COMP90043 (2012) written by kbawa, ankita, nqngo.
===================================================================
Please read the accompanied README file before use.
To see all commands, please type :help.

Quick start guide
=================
To start decrypting your ciphertext, type:
>:solve <ciphertext>
To test the decryption with a given plaintext, type:
>:test <plaintext>
The program will automatically decrypt the plaintext and solve.

Adjust the 'settings.ini' and restart the program to adjust different
parameters of the program. To set it on the fly, use :set command, see
:help for further instruction.
\n"""

# Help message
help = """Program commands:
    :help -- show help commands.
    :quit -- terminate the program.
    :env <module> -- list the module configuration. If no module specified,
                     list all configuration. 
    :set <module>.<attribute> <value> -- set the settings of the corresponding
                                         attribute in the module to specific value.
    :encrypt <plaintext> -- encrypt a plaintext with random mono-substitution cipher
                            key and return the result.
    :solve <ciphertext> -- solve the ciphertext.
    :test <plaintext> -- this is similar to running :encrypt <plaintext> then
                         :solve the resulting ciphertext.\n"""
                        
# Change locale message
setLocale = "Unravel switched to English interface."

# Terminate message
exit = """Program terminated...Goodbye."""

# Error message
setError = "Error: Usage -- :set <module>.<field> <value>"
envError = "Error: No such module name. See settings.ini for module name."
