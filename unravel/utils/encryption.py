#!/usr/bin/env python
# encoding: utf-8

"""
Ulitility function to encrypt a ciphertext.

Written by nqngo, kbawa (2012).
"""
import random

def _preprocess(text, alphabets):
    """Preprocess a plaintext."""
    return ''.join(c for c in text if c in unicode(alphabets)).lower()


def encrypt(plaintext, cipherglyph, alpha, key=None):
    """
    Encrypt a plaintext to ciphertext using a given key. If
    None is specified, scramble a random key"""
    cleantext = _preprocess(plaintext, cipherglyph + alpha)
    if key is None:
        ralpha = list(alpha)
        random.shuffle(ralpha)
        ralpha = ''.join(ralpha)
        key = dict(zip(unicode(ralpha), unicode(cipherglyph)))
    return ''.join(key[c] for c in cleantext)

