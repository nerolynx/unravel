#!/usr/bin/env python
# encoding: utf-8
"""
Ultility classes to build statistic from
Europarl source corpus.

Written nqngo (2012)
"""
import os
import time
import json
import nltk
import codecs
import multiprocessing
from multiprocessing import Queue, Process
from Queue import Empty as QueueEmpty

def slowbuild(dirin, dirout, alpha):
    """Build corpus single core."""
    fd1 = nltk.FreqDist()
    fd2 = nltk.FreqDist()
    #fd3 = nltk.FreqDist()
    #fd4 = nltk.FreqDist()
    for fn in os.listdir(dirin):
        with codecs.open(os.path.join(dirin, fn), encoding="utf-8") as f:
            lines = [line for line if line[0] is not in f.readlines()]
            for line in f.readlines():
                txt = clean(line, alpha)
                #fd1.update(txt)
                #fd2.update(''.join(w) for w in nltk.bigrams(txt))
                #fd3.update(''.join(w) for w in nltk.trigrams(txt))    
                fd4.update(''.join(w) for w in nltk.ngrams(txt, 4))
    freq4 = dict((k, fd4.freq(k)) for k in fd4)
    export(dirout, "europarl4.json", freq4)
    # freq1 = dict((k, fd1.freq(k)) for k in fd1)
    #     export(dirout, "europarl1.json", freq1)
    #     freq2 = dict((k, fd2.freq(k)) for k in fd2)
    #     export(dirout, "europarl2.json", freq2)
    #     freq3 = dict((k, fd3.freq(k)) for k in fd3)
    #     export(dirout, "europarl3.json", freq3)


def build(dirin, dirout, alpha, CPU = 1):
    """Build the corpus based on dir."""
    qlines = Queue()
    qstat = Queue()
    qstat2 = Queue()
    qstat3 = Queue()
    
    alpha = set(list(alpha))
    
    if CPU == 1:
        try:
            CPU = multiprocessing.cpu_count()
            if __DEBUG__:
                print CPU
        except NotImplementedError:
            pass
    
    r = Process(target=read, args=(dirin, qlines, ))
    r.start()
    
    processes = [Process(target=process,
                args=(qlines, qstat, alpha, ))
                for cpu in range(CPU)]
    for p in processes:
        p.start()

    for p in processes:
        p.join(timeout=10)

    fd1 = nltk.FreqDist()
    fd2 = nltk.FreqDist()
    fd3 = nltk.FreqDist()
    
    while True:
        
        try:
            print "Try something"
            r1 = qstat.get(timeout=1)
            print r1
            #[fd1.inc(k, d) for (k,d) in r1]
        except QueueEmpty:
            return
            #freq1 = dict((k, fd1.freq(k)) for k in fd1)
            #export(dirout, "europarl1.json", freq1)
        except:
            raise
    
        # try:
        #            r2 = qstat2.get(timeout=1)
        #            print r2
        #            [fd2.inc(k, d) for (k,d) in r2]
        #        except QueueEmpty:
        #            freq2 = dict((k, fd2.freq(k)) for k in fd2)
        #            export(dirout, "europarl2.json", freq2)
        #        except:
        #            raise
        # 
        #        try:    
        #            r3 = qstat3.get(timeout=1)
        #            [fd3.inc(k, d) for (k,d) in r3]
        #        except QueueEmpty:
        #            freq3 = dict((k, fd3.freq(k)) for k in fd3)            
        #            export(dirout, "europarl3.json", freq3)
        #            return
        #        except:
        #            raise


def read(corpus, qout):
    """Read in all files in a directory."""
    for fn in os.listdir(corpus):
        with codecs.open(os.path.join(corpus, fn), encoding='utf-8') as f:
            [qout.put(line) for line in f.readlines()]


def process(qin, qstat, alpha):
    """Process all file in a queue."""
    fd1 = nltk.FreqDist()
    fd2 = nltk.FreqDist()
    fd3 = nltk.FreqDist()
    while True:
        try:
            line = qin.get(timeout=1)
            if line[:1] in "<":
                continue
            txt = clean(line, alpha)
            fd1.update(txt)
            fd2.update(''.join(w) for w in nltk.bigrams(txt))
            fd3.update(''.join(w) for w in nltk.trigrams(txt))
        except QueueEmpty:
            qstat.put((fd1.items(), fd2.items(), fd3.items()))
            return
        except:
            raise

def clean(line, alpha):
    """Clean each input line."""
    return ''.join(c for c in line if c in alpha).lower()


def export(dir, name, obj):
    """Export an object into a JSON file at given directory."""
    if not os.path.exists(dir):
        os.mkdir(dir)
    with open(os.path.join(dir, name), 'w') as f:
        json.dump(obj, f, sort_keys=True, indent=2)

__DEBUG__ = True

if __DEBUG__ == True:
	corpus = '../../data/en'
	fileout = '../../assets'
	alpha = u"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	start = time.time()
	slowbuild(corpus, fileout, alpha)
	print "Time taken %.4f seconds." % (time.time() - start)
