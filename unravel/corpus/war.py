#!/usr/bin/env python
# encoding: utf-8
"""
Ultility class to build statistic from
War and Peace epic novel.

Written by nqngo

"""
import os
import json
import time
import codecs
import nltk

def build(infile, outdir, alpha):
    """Build the corpus data."""
    # Open the file and clean the text
    with codecs.open(infile, encoding='utf-8') as src:
        txt = ''.join(c for c in src.read() if c in alpha).lower()

    fd1 = nltk.FreqDist(txt)
    fd1 = dict((k, fd1.freq(k)) for k in fd1)
    toJson(outdir, "war1.json", fd1)
    
    fd2 = nltk.FreqDist(''.join(e) for e in nltk.bigrams(txt))
    fd2 = dict((k, fd2.freq(k)) for k in fd2)
    toJson(outdir, "war2.json", fd2)
    
    fd3 = nltk.FreqDist(''.join(e) for e in nltk.trigrams(txt))
    fd3 = dict((k, fd3.freq(k)) for k in fd3)
    toJson(outdir, "war3.json", fd3)
    
    fd4 = nltk.FreqDist(''.join(e) for e in nltk.ngrams(txt, 4))
    fd4 = dict((k, fd4.freq(k)) for k in fd4)
    toJson(outdir, "war4.json", fd4)

    
def toJson(outdir, name, obj):
    """Export an object into a JSON file at given directory."""
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    with open(os.path.join(outdir, name), 'w') as f:
        json.dump(obj, f, sort_keys=True, indent=2)

#  Should move to test/
# __DEBUG__ = False
# 
# if __DEBUG__ == True:
#     corpus = '../../data/gutenberg/war.txt'
#     outdir = '../../assets'
#     alpha = u"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#     start = time.time()
#     build(corpus, outdir, alpha)
#     print "Time taken %.4f seconds." % (time.time() - start)
