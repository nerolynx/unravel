#!/usr/bin/env python
# encoding: utf-8

"""
The main program and controller for unravel

Written by nqngo, kbawa (2012)
"""

import interfaces.cli
import solvers.hillClimbing
import utils.encryption as enc

def parseConfig(file):
    """Parse the configuration from the given file."""
    f = open(file, 'rb')
    raw = f.readlines()
    f.close()
    
    ignoreTokens = ['#','\n','\r']
    params = {}
    
    for line in raw:
        # Sanitise the line
        line = line.lstrip(' ')
        if line[0] in ignoreTokens:
            continue
        if line[0] == '[':
            heading = line.strip('[]\n')
            params[heading] = {}
            current = params[heading]
        else:
            setting = [side.strip(' \n') for side in line.split('=')]
            current[setting[0]] = setting[1]
    return params


# settings ini location
params = parseConfig("unravel/settings.ini")
# Start the view
cli = interfaces.cli.View(params['GENERAL']['locale'])
# Start the controller
controller = interfaces.cli.Controller(params, cli)
# Solver
solver = solvers.hillClimbing.Solver(params['HILLCLIMBING'],
                                     params['GENERAL']['alphabet'],
                                     params['GENERAL']['cipherglyph'])
# Main loop
while True:
    query = cli.view()
    print query
    if query[0] == 'V':
        controller.envParams(query[1])
        continue
    if query[0] == 'Q':
        solver.solve(query[1])
        continue
    if query[0] == 'X':
        print enc.encrypt(query[1], params['GENERAL']['cipherglyph'],
                    params['GENERAL']['alphabet'])
        continue
    if query[0] == 'T':
        solver.solve(enc.encrypt(query[1],
                                 params['GENERAL']['cipherglyph'],
                                 params['GENERAL']['alphabet']))
        continue
    if query[0] == 'S':
        params[query[1]][query[2]] = query[3]
        continue
