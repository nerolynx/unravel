#!/usr/bin/env python
# encoding: utf-8
"""
Commandline interface for Unravel solver.
Written by nqngo (2012)
"""

class View(object):
    """The command line interface to Unravel"""
    
    def __init__(self, lang):
        """Welcome message."""
        mod = __import__("langs", fromlist =[lang])
        self.locale = getattr(mod, lang)
        print self.locale.welcome


    def __help(self):
        """Printing help instruction"""
        print self.locale.help
        return ("H")
                              

    def __exit(self):
        """Terminate the program"""
        print self.locale.exit
        exit()


    def __setParams(self, stringin):
        """Set the parameters from string in."""
        inp = stringin.rsplit(':set')[1].strip(' \n').split(' ')
        if len(inp) > 1:
            tag, value = inp[0], ' '.join(inp[1:])
        else:
            print self.locale.setError
            return ("E")
        inp = tag.split('.')
        if len(inp) > 1:
            module, attr = inp[0], '.'.join(inp[1:])
        else:
            print self.locale.setError
            return ('E')
        return ('S', module, attr, value)


    def __setlocale(self, lang):
        """Set the program language."""
        mod = __import__("langs", fromlist =[lang])
        self.locale = getattr(mod, lang)
        print self.locale.setLocale
        return ("L")

    
    def __envParams(self, stringin):
        """List all the program env variable."""
        inp = stringin.rsplit(":env")[1].strip(' \n').split(' ')
        if len(inp) > 1:
            print self.locale.envError
            return ("E")
        return ("V", inp[0])


    def __encrypt(self, stringin):
        """Encrypt the functions."""
        inp = stringin.rsplit(":encrypt")[1]
        if inp:
            return ("X", inp)
        return ("E")
    
    def __test(self, stringin):
        """Test the functions"""
        inp = stringin.rsplit(":test")[1]        
        if inp:
            return ("T", inp)
        return ("E")

    def __solve(self, stringin):
        """Solve the functions"""
        inp = stringin.rsplit(":solve")[1]
        if inp:
            return ("Q", inp)
        return ("E")   

    def view(self):
        """Present the commandline interface view and handle input."""
        buff = raw_input(">")

        # If empty
        if not buff:
            return ("E")
        # If quit
        if ":quit" in buff:
            self.__exit()
        # If help
        if ":help" in buff:
            return self.__help()
        # If set params
        if ":set" in buff:
            return self.__setParams(buff)
        # if env params
        if ":env" in buff:
            return self.__envParams(buff)
        # If encrypt
        if ":encrypt" in buff:
            return self.__encrypt(buff)
        # If solve
        if ":solve" in buff:
            return self.__solve(buff)
        # If test inputs
        if ":test" in buff:
            return self.__test(buff)
        # If specified a file, return it
        if ":file" in buff:
            return ("F", buff.split(":file")[1])
        # Otherwise, return the ciphertext
        return ("E")


class Controller(object):
    """The controller for Unravel"""
    
    def __init__(self, params, view):
        """Initialise the controller"""
        self._params = params
        self.view = view
    

    def envParams(self, module):
        """
        List the settings for the modules. If none
        specified, list everything."""
        if not module:
            for module in self._params:
                self.envParams(module)
        else:
            if module not in self._params:
                print self.view.locale.envError
                return
            print '[%s]' % module
            for key in self._params[module]:
                print '%s = %s' % (key, self._params[module][key])


