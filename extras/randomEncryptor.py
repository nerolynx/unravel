import random

#generate random secret key
alphabets = map(chr, range(97,123))
keys = random.shuffle(alphabets)

key_dict = dict(zip(alphabets, map(chr, range(97,123))))
print key_dict

ciphertext = []
plaintext = "inputthedatayouwanthereplease"

#convert with plain text to cipher text
for letter in plaintext:
    ciphertext.append(key_dict[letter])

print ''.join(ciphertext)
