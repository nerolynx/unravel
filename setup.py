from distutils.core import setup
import sys

sys.path.append('unravel')
import unravel


setup(name='unravel',
      version='0.2',
      author='Nhat Ngo, Kunal Bawa',
      author_email='nqngo@student.unimelb.edu.au, kbawa@student.unimelb.edu.au',
      url='https://bitbucket.org/nerolynx/unravel',
      download_url='https://bitbucket.org/nerolynx/unravel',
      description='An end-to-end solver for monosubstitution ciphers using statistical methods. ',
      long_description=unravel.unravel.__doc__,
      package_dir={'': 'googlemaps'},
      py_modules=['unravel'],
      provides=['unravel'],
      keywords='unravel crytography monosubstitution cipher statistical',
      license='FreeBSD License',
      classifiers=['Development Status :: 1 - Development/Unstable',
                   'Intended Audience :: Developers',
                   'Natural Language :: English',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 2',
                   'License :: OSI Approved :: FreeBSD License',
                   'Topic :: Crytography',
                   'Topic :: Crytography :: Monosubstitution cipher',
                  ],
     )
