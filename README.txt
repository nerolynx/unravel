UNRAVEL : An Automated Cryptanalysis Program for Mono-alphabetic Subsitution Cipher
----------------
Copyright nqngo, ankita, kbawa (2012).
Department of Computing and Information Systems
University of Melbourne, Victoria, Australia.
*******************************************************************************

=-=-=-=-=-=-=-=-=-=-=
1. Software License
=-=-=-=-=-=-=-=-=-=-=

This software is distributed under permissive BSD License. For the full
license please refer to the accompanied LICENSE file.
	
=-=-=-=-=-=-=-=-=-=-=
2. Overview
=-=-=-=-=-=-=-=-=-=-=
:
UNRAVEL is an automated cryptanalysis program to break mono-substitution cipher,
it is devised as an experimental framework to be extensible to fit on new technique
of break mono-substitution and designed to be highly modular and extendable using
Python scripting language. As a prototype, it is built to allow ease of extension
and deployment via Python scripts and command line interface.

The prototype allows users to feed sample ciphertext and plaintext source and
experiment with different solving techniques, allowing multiple configure settings
and ease the research into language and cipher processing. The program supported
Unicode and could be easily customised to run in different language.

In order to achieve the above, only a few commands have to be adhered. This greatly
simplifies the task of checking the outcome of each solving method for the users.

=-=-=-=-=-=-=-=-=-=-=
3. Installation
=-=-=-=-=-=-=-=-=-=-=

Platform compatibility:
    *nix-like system, Windows (untested)
    
    Unravel is written as a commandline tools. While the underlying dependencies
    support 

Prerequisite: Python 2.6+, NLTK 2.0+

For instruction how to each dependency, please refer to the following guide:
	1. Python : http://www.python.org/getit/
	2. NTLK   : http://nltk.org/install.html

Compatibility:
	- Python 2.6-2.7
	- NLTK 2.0.1-2.0.3

Installation instruction:
    1. Install all dependencies;
	2. Download and unpack the source.
	
Start the software:
	1. Adjust the settings.ini to match the parameters needed to run;
	2. python unravel and follow the interactive shell;
	3. For help run :help.

=-=-=-=-=-=-=-=-=-=-=
4. Usage Instruction
=-=-=-=-=-=-=-=-=-=-=

Unravel can be customised and driven entirely from commandline. When the program
is started enter command into the instruction terminal, denoted as followed:
>

To solve a monosubstitution cipher, enter ":solve" followed by the ciphertext.
For example, with the ciphertext "ASOIAUSP". Type:
>:solve ASOIAUSP

The program will attempt to solve the cipher based on the settings parsed from
"settings.ini" file. To encrypt a plain text using mono-substitution cipher. Type
":encrypt" followed by the given plain text. For example, to encrypt the plain text
"Hello World!". Type:
>:encrypt Hello World!

To test the algorithm using a plaintext. Simple use ":test" command, this will
automatically encrypt and attempt to solve the plaintext. For example, with the
above plain text "Hello World!", use:
>:test Hello World!

To see all current parameters in the program, type ":env":
>:env

Otherwise, if you wish to find a specific module settings, type ":env" followed
by the module name. Assume you wish to see the settings for HILLCLIMBING module.
Type:
>:env HILLCLIMBING

Similarly, the settings can be adjusted on the fly. Simply by using the ":set"
instruction. For example, to change the fitness function of HILLCLIMBING module
to "bigram" fitness function. Use ":set" followed by the module name (more info
below), the settings field - separated by a dot (.) - and the value to change: 
>:set HILLCLIMBING.fitness 2gram 

For more usage command, please refer to the ":help" instruction.

=-=-=-=-=-=-=-=-=-=-=
5. Customisation
=-=-=-=-=-=-=-=-=-=-=

Most allowed customisation option is provided in "settings.ini".  In which,
the following variables are particularly important:

A. GENERAL module: settings related to the main program.
	alphabet -- the alphabet of the plaintext.
	cipherglyph -- the alphabet of the ciphertext.
	locale -- the language which the program is operated on. The following
	          language is currently supported: en

B. HILLCLIMBING module: settings for the hill-climbing solver.
    iteration -- how many iteration the program is ran.
    stat1f -- the location of the JSON file store probability distribution for
              unigram from given corpus.
    stat2f -- similarly, the location of probability distribution JSON for bigrams.
    stat3f -- similarly, but for trigrams.
    stat4f -- similarly, for for tetragrams.
    floor  -- the floor for the log probability of each n-gram functions. Instead
              of -inf, this value should set to some abitrary low number to speed
              up calculation.
	fitness -- the log fitness function used in hillclimbing algorithm. Available
	           options are: 2gram, 3gram, 4gram.

=-=-=-=-=-=-=-=-=-=-=
6. Extension
=-=-=-=-=-=-=-=-=-=-=

Unravel is built as an extendable prototype framework. The program composed of the
following modules:

unravel                             < the main program
 +interfaces                        < different interfaces
 +langs                             < translation file
 +corpus                            < scripts to build corpus data
 +solvers                           < script for different solvers
 +utils                             < utility functions

To add new translation to the program. It is simple enough to provide a .py file
contain all the translation variable (taken from en.py) and set the locale according.

To gather statistical data for a corpus, sample scripts are written and placed
in corpus. If the user wants to extend a new corpus, simply write a new script
based on the provided war.py (for War and Peace novel as a corpus) or europarl.py
(for Europarl corpus) and point it at the right file(s). The resulting corpus
data should be save as a dictionary JSON file in assets/ folder.

To provide new interfaces for Unravel, simply extend and write new interface for
the program. Unravel is designed follow an MVC model, where interface is the View
and __main__.py is the controller. Unravel is also designed using a messaging
paradigm, so even with the interface change alone. It should be relatively easy
to repurpose the __main__.py settings.

To add new solvers, add solver class to solvers package and send the parameters
at runtime.
